 comm=$(git log --date=short |grep commit |head -qn1 |sed 's/commit//' | cut -c 1-9 |sed 's/ //g' )
 date=$(git log --date=short | grep Date | sed 's/.*:  //  ' |head -qn1 | sed  's/-//g' |sed 's/ //g')
 export version=$date$comm
 mvn versions:set -DnewVersion=$version
 echo $version